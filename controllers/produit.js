const Produit = require('../models/Produit');
//
////////////
// CREATE//
//////////
//
exports.createProduit = (req, res) => {
    const produit = req.body
    const produitOk = new Produit({
        ...produit
    })
    console.log(produitOk);
    produitOk.save().then(result => {
        res.status(201).json({
            message: "Produit crée"
        })
    }).catch(err => {
    res.status(400).json({message: "Une erreur est survenue"})
})
}
//
////////////
//  GET  //
//////////
//
exports.getProduits = (req, res) => {
    Produit.find().then(produits => {
        res.status(200).json(produits);
    }).catch(err => {
        res.status(400).json(err);
    })
}
//
//////////////////
//  GET BY ID  //
////////////////
//
exports.getProduitById = (req, res) => {
    Produit.findById(req.params.id).then(produit => {
        res.status(200).json(produit);
    }).catch(err => {
        res.status(400).json(err);
    })
}
//
///////////////
//  UPDATE  //
/////////////
//
exports.updateProduit = (req, res) => {
    const produitUpdated = new Produit({
        ...req.body
    })
    Produit.findByIdAndUpdate(req.params.id, produitUpdated).then(result => {
        res.status(201).json({message: "Le produit a ete mis a jour"})
    }).catch(err => {
        res.status(400).json(err)
    })
}
//
///////////////
//  DELETE  //
/////////////
//
exports.deleteProduit = (req, res) => {
    Produit.findByIdAndDelete(req.params.id).then(() => {
        res.status(201).json({message: "Le produit a ete suprime"})
    }).catch(err => {
        res.status(400).json(err)
    })
}