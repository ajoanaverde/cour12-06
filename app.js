//
const express = require('express');
// express sert pour les routes et les middlewhere
// ça sert a etablir la comunication au sein d'une application
const app = express();
// ici on fait de notre apli une apli express
const bodyParser = require('body-parser')
//
const mongoose = require('mongoose');
//
const produitsController = require('./controllers/produit');
//
mongoose.connect('mongodb://localhost:27017/aula', {
    useNewUrlParser: true
}).then(() => {
    console.log("Connection mongodb ok")
}).catch(err => {
    console.log(err)
})
//
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
//
// ROUTES
// attencion : l'ordre des routes est importante
//
app.delete('/produits/:id', produitsController.deleteProduit);
app.put('/produits/:id', produitsController.updateProduit);
app.get('/produits/:id', produitsController.getProduitById);
app.post('/produits', produitsController.createProduit);
app.get('/produits', produitsController.getProduits);
//
//
// ça exporte le fichier pour pouvoir l'utiliser ailleurs 
module.exports = app;
//
// j'ai oublié:
// titre; contenue; created at; apdated at; 
//
//
// dans une api il y a pas des vues
// un mvc classique, les 3 parties sont emsemble 
// una api, le front et le back sont completement separes
// une api est aussi plus secure
//
// web service:
// 